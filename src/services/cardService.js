import fs from "fs"
import path from "path"
import camelCase from "lodash/camelCase"

export function csvToJson(file) {
  const [headerLine, ...lines] = file.split("\n")
  const headers = headerLine.split(";")
  return lines.map(line => {
    const cells = line.split(";")
    const tmpObject = {}
    for (let i = 0; i < cells.length; i++) {
      tmpObject[camelCase(headers[i])] = Number.parseInt(cells[i])
    }
    return tmpObject
  })
}

export async function importBuildings() {
  const buildingsPath = path.join(__dirname, "../ressources/buildings.csv")
  const buildingsFile = await fs.promises.readFile(buildingsPath, "utf8")
  // L'exception n'est pas catché donc elle va remonter si elle est levée.
  return csvToJson(buildingsFile)
}

export async function importWorkers() {
  const workersPath = path.join(__dirname, "../ressources/workers.csv")
  const workersFile = await fs.promises.readFile(workersPath, "utf8")
  // L'exception n'est pas catché donc elle va remonter si elle est levée.
  return csvToJson(workersFile)
}
