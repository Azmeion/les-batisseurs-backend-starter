import request from "supertest"
import app from "../app"
import fs from "fs"

jest.mock("fs")

const testCsv = `\
wood;stone;victory point
1;2;3
4;5;6`

describe("/cards/workers", () => {
  test("should response the GET method", async () => {
    fs.promises = {
      readFile: jest.fn().mockResolvedValue(testCsv)
    }

    const response = await request(app).get("/cards/workers")
    expect(response.statusCode).toBe(200)
    expect(response.body).toStrictEqual([
      { wood: 1, stone: 2, victoryPoint: 3 },
      { wood: 4, stone: 5, victoryPoint: 6 }
    ])
  })

  test("should return 500 if reading failed", async () => {
    fs.promises = {
      readFile: jest.fn().mockRejectedValue(Error("Error test"))
    }
    const response = await request(app).get("/cards/workers")
    expect(response.statusCode).toBe(500)
    expect(response.text).toEqual("Can't read workers cards.")
  })
})

describe("/cards/buildings", () => {
  test("should response the GET method", async () => {
    fs.promises = {
      readFile: jest.fn().mockResolvedValue(testCsv)
    }
    const response = await request(app).get("/cards/buildings")
    expect(response.statusCode).toBe(200)
    expect(response.body).toStrictEqual([
      { wood: 1, stone: 2, victoryPoint: 3 },
      { wood: 4, stone: 5, victoryPoint: 6 }
    ])
  })

  test("should return 500 if reading failed", async () => {
    fs.promises = {
      readFile: jest.fn().mockRejectedValue(Error("Error test"))
    }
    const response = await request(app).get("/cards/buildings")
    expect(response.statusCode).toBe(500)
    expect(response.text).toEqual("Can't read buildings cards.")
  })
})
