import cardRouter from "./cardRouter"
import healthRouter from "./healthRouter"
import express from "express"

const router = express.Router()

router.use("/cards", cardRouter)
router.use("/health", healthRouter)

export default router
